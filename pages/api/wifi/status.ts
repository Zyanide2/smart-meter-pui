// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import * as wifi from 'node-wifi';
import {WifiNetworkDto} from "../../../dto/WifiNetworkDto";
import {ResponseDto} from "../../../dto/ResponseDto";
import * as _404handler from "../[[...404]]";
import os from "os";

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<WifiNetworkDto>>
) {
    if(req.method != "GET") return _404handler.default(req, res);

    wifi.init({
        iface: os.platform() === 'linux' ? 'wlan0' : undefined
    });
    return wifi.getCurrentConnections()
        .then(networks => {
            const net = networks[0]
            if(!net) return res.status(404).json({
                status: 404,
                ok: true,
                message: "Device is not connected"
            });

            res.status(200).json({
                status: 200,
                ok: true,
                body: {
                    ssid: net.ssid,
                    quality: net.quality,
                    hasSecurity: !!net.security
                }
            });
        })
        .catch(err => {
            res.status(500).json({
                status: 500,
                ok: false,
                message: err,
                error: 'Internal Server Error'
            });
        });
}
