// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import * as wifi from 'node-wifi';
import {WifiNetworkDto} from "../../../dto/WifiNetworkDto";
import {ResponseDto} from "../../../dto/ResponseDto";
import * as _404handler from "../[[...404]]";
import {ConnectionOpts} from "node-wifi";
import os from "os";

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<undefined>>
) {
    if(req.method != "POST") return _404handler.default(req, res);
    try {
        const body: ConnectionOpts = JSON.parse(req.body);

        wifi.init({
            iface: os.platform() === 'linux' ? 'wlan0' : undefined
        });
        return wifi.connect({
            ssid: body.ssid,
            password: body.password
        })
            .then(() => {
                res.status(200).json({
                    status: 200,
                    ok: true,
                    message: 'Connected successfully'
                });
            })
            .catch(err => {
                res.status(500).json({
                    status: 500,
                    ok: false,
                    message: err,
                    error: 'Internal Server Error'
                });
            });
    } catch (e) {
        return res.status(400).json({
            status: 400,
            ok: false,
            message: 'The request body is not a valid JSON',
            error: 'Internal Server Error'
        });
    }
}
