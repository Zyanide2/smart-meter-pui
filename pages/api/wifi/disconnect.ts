// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import * as wifi from 'node-wifi';
import {WifiNetworkDto} from "../../../dto/WifiNetworkDto";
import {ResponseDto} from "../../../dto/ResponseDto";
import * as _404handler from "../[[...404]]";
import os from "os";

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<undefined>>
) {
    if(req.method != "POST") return _404handler.default(req, res);

    wifi.init({
        iface: os.platform() === 'linux' ? 'wlan0' : undefined
    });
    return wifi.disconnect()
        .then(() => {
            res.status(200).json({
                status: 200,
                ok: true,
                message: 'Disconnected successfully'
            });
        })
        .catch(err => {
            res.status(500).json({
                status: 500,
                ok: false,
                message: err,
                error: 'Internal Server Error'
            });
        });
}
