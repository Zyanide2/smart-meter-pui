// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import * as wifi from 'node-wifi';
import {WifiNetworkDto} from "../../../dto/WifiNetworkDto";
import {ResponseDto} from "../../../dto/ResponseDto";
import * as _404handler from "../[[...404]]";
import os from "os";

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<WifiNetworkDto[]>>
) {
    if(req.method != "GET") return _404handler.default(req, res);

    wifi.init({
        iface: os.platform() === 'linux' ? 'wlan0' : undefined
    });
    return wifi.scan()
        .then(networks => {
            res.status(200).json({
                status: 200,
                ok: true,
                body: networks.map<WifiNetworkDto>(net => {
                    return {
                        ssid: net.ssid,
                        quality: net.quality,
                        hasSecurity: !!net.security
                    };
                })
            });
        })
        .catch(err => {
            res.status(500).json({
                status: 500,
                ok: false,
                message: err,
                error: 'Internal Server Error'
            });
        });
}
