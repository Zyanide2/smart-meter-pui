// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import {ResponseDto} from "../../dto/ResponseDto";

type Data = ResponseDto<any>;

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
) {
    return res.status(404).send({
        status: 404,
        ok: false,
        message: 'The requested route was not found',
        error: 'Not Found'
    });
}
