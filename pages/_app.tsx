import '../styles/globals.css'
import type {AppProps} from 'next/app'
import Keyboard from "react-simple-keyboard";
import {useEffect, useState} from "react";
import styles from './_app.module.css';
import 'react-simple-keyboard/build/css/index.css';
import {KeyboardManager} from "../utils/KeyboardManager";

function MyApp({Component, pageProps}: AppProps) {
    const [keyboardLayout, setKeyboardLayout] = useState<string>('default')
    const [keyboardVisibility, setKeyboardVisibility] = useState<boolean>(false);
    const clickHandler = (e: any) => {
        if (
            KeyboardManager.getRetractAllowedAfter() < KeyboardManager.getTimestamp() &&
            e.target.nodeName !== "INPUT" &&
            !e.target.classList.contains("hg-button")
        ) KeyboardManager.hideKeyboard();
    }

    useEffect(() => {
        KeyboardManager.subscribeKeyboardVisibilityChanged(setKeyboardVisibility);
        window.addEventListener("click", clickHandler);
        return window.removeEventListener("click", clickHandler, true);
    }, [])

    return (
        <div className={styles['container']}>
            <Component {...pageProps} />
            <div className={`${styles['keyboard-container']} ${keyboardVisibility ? styles['keyboard-expanded'] : styles['keyboard-collapsed']}`}>
                <Keyboard
                    keyboardRef={KeyboardManager.getRef}
                    layoutName={keyboardLayout}
                    onChange={KeyboardManager.onChange}
                    onKeyPress={(btn: any) => {
                        KeyboardManager.manageLayout(btn, keyboardLayout, setKeyboardLayout);
                        KeyboardManager.isEnterPressed(btn);
                    }}
                />
            </div>
        </div>
    );
}

export default MyApp
