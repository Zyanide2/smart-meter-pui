import type {NextPage} from 'next'
import {useEffect, useState} from "react";
import {WifiNetworkDto} from "../dto/WifiNetworkDto";
import CurrentNetworkPanel from "../components/CurrentNetworkPanel";
import DetectedNetworkList from "../components/DetectedNetworkList";
import {WifiManager} from "../utils/WifiManager";
import {KeyboardManager} from "../utils/KeyboardManager";

const Home: NextPage = () => {
    const [detectedNetworks, setDetectedNetworks] = useState<WifiNetworkDto[]>([]);
    const [currentNetwork, setCurrentNetwork] = useState<WifiNetworkDto | null>();
    const refreshNetworkDataInterval = 3000; // in ms
    const refreshNetworkData = () => {
        if(!currentNetwork) WifiManager.updateDetectedNetworks();
        WifiManager.updateCurrentNetwork();
    }

    useEffect(() => {
        WifiManager.subscribeDetectedNetworksChanged(networks => {
            if(!KeyboardManager.getKeyboardVisibility()) setDetectedNetworks(networks);
        });
        WifiManager.subscribeCurrentNetworkChanged(currentNetwork => {
            if(!KeyboardManager.getKeyboardVisibility()) setCurrentNetwork(currentNetwork);
        });
        WifiManager.subscribeConnectedToWifi(ok => {
            if(!ok) return alert('Could not connect!');
        });
        WifiManager.subscribeDisconnectFromWifi(ok => {
            if(!ok) return alert('Could not disconnect!');
        });

        refreshNetworkData();
        setInterval(refreshNetworkData, refreshNetworkDataInterval);
    }, []);

    return (
        <div>
            {!currentNetwork
                ? <DetectedNetworkList detectedNetworks={detectedNetworks} />
                : <CurrentNetworkPanel {...currentNetwork} />
            }
        </div>
    );
}

export default Home;
