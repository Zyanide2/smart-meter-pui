export class KeyboardManager {
    private static _keyboardVisible: boolean = false;
    private static _keyboardRetractAllowedAfter: number = KeyboardManager.getTimestamp();
    private static _setter: (input: string)=>void = ()=>{};
    private static _ref: any;

    public static showKeyboard(setter: (input: string)=>void) {
        KeyboardManager._keyboardVisible = true;
        KeyboardManager._setter = setter;
        KeyboardManager._keyboardRetractAllowedAfter = KeyboardManager.getTimestamp()+1000;
        KeyboardManager.callEvent(KeyboardManager._keyboardVisibilityChangedEvent, [KeyboardManager._keyboardVisible]);
    }

    public static hideKeyboard() {
        KeyboardManager._keyboardVisible = false;
        KeyboardManager._keyboardRetractAllowedAfter = KeyboardManager.getTimestamp()+1000;
        KeyboardManager._ref.clearInput();
        KeyboardManager.callEvent(KeyboardManager._keyboardVisibilityChangedEvent, [KeyboardManager._keyboardVisible]);
    }

    public static isEnterPressed(btn: string) {
        if(btn === '{enter}') {
            KeyboardManager._enterIsPressedEvent.forEach(callback => callback());
            return true;
        }

        return false;
    }

    public static getRef(ref: any) {
        KeyboardManager._ref = ref;
    }

    public static onChange(input: string) {
        KeyboardManager._setter(input);
    }

    public static manageLayout(button: string, currentLayout: string, layoutSetter: (layout: string)=>void) {
        if (button === "{shift}" || button === "{lock}") layoutSetter(currentLayout === "default" ? "shift" : "default");
    }

    public static getTimestamp() {
        return new Date().getTime();
    }

    public static getKeyboardVisibility = () => KeyboardManager._keyboardVisible;
    public static getRetractAllowedAfter = () => KeyboardManager._keyboardRetractAllowedAfter;

    private static callEvent = (event: ((...params: any)=>void)[], data: any = []) => event.forEach(e => e(...data));
    private static _keyboardVisibilityChangedEvent: ((visible: boolean)=>void)[] = [];
    private static _enterIsPressedEvent: (()=>void)[] =[];
    public static subscribeKeyboardVisibilityChanged = (callback: (visible: boolean)=>void) => KeyboardManager._keyboardVisibilityChangedEvent.push(callback);
    public static subscribeEnterIsPressed = (callback: (()=>void)) => KeyboardManager._enterIsPressedEvent.push(callback);
    public static resetEnterIsPressedSubscriptions = () =>  KeyboardManager._enterIsPressedEvent = [];
}