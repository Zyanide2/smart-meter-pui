import {WifiNetworkDto} from "../dto/WifiNetworkDto";
import {ResponseDto} from "../dto/ResponseDto";
import {ConnectionOpts} from "node-wifi";

export class WifiApi {

    public static connect(ssid: string, password: string, callback: (ok: boolean)=>void): void {
        WifiApi.request<ResponseDto<WifiNetworkDto[]>, ConnectionOpts>('wifi/connect', 'POST', {
            ssid,
            password
        })
            .then(res => {
                callback(res.ok);
            })
            .catch(err => {
                callback(false);
            });
    }

    public static disconnect(callback: (ok: boolean)=>void): void {
        WifiApi.request<ResponseDto<WifiNetworkDto[]>>('wifi/disconnect', 'POST')
            .then(res => {
                callback(res.ok);
            })
            .catch(err => {
                callback(false);
            });
    }

    public static getDetectedNetworks(callback: (data?: WifiNetworkDto[])=>void): void {
        WifiApi.request<ResponseDto<WifiNetworkDto[]>>('wifi/scan')
            .then(res => {
                callback(res.body);
            })
            .catch(err => {
                callback();
            });
    }

    public static getCurrentNetwork(callback: (data?: WifiNetworkDto|null)=>void): void {
        WifiApi.request<ResponseDto<WifiNetworkDto>>('wifi/status')
            .then(res => {
                if(res.status == 404) return callback(null);
                callback(res.body);
            })
            .catch(err => {
                callback();
            });
    }

    private static request<TResponse, TBody=any>(url: string, method: string = 'GET', body?: TBody): Promise<TResponse> {
        return fetch('http://localhost:3000/api/'+url, {
            method,
            cache: 'no-cache',
            headers: {
                'Content-Type': 'json/application'
            },
            body: body ? JSON.stringify(body) : undefined
        })
        .then(res => res.json())
        .catch(res => res.json());
    }
}