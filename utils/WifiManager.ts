import {WifiNetworkDto} from "../dto/WifiNetworkDto";
import {WifiApi} from "./WifiApi";

export class WifiManager {
    private static _detectedNetworks: WifiNetworkDto[] = [];
    private static _currentNetwork: WifiNetworkDto|null = null;

    public static updateDetectedNetworks(callback?: (ok: boolean, network?: WifiNetworkDto[])=>void) {
        WifiApi.getDetectedNetworks(data => {
            if (!data) {
                if(callback) callback(false);
                return;
            }

            WifiManager._detectedNetworks = data;
            if(callback) callback(true, WifiManager._detectedNetworks);
            WifiManager.callEvent(WifiManager._detectedNetworksChangedEvent, [WifiManager._detectedNetworks]);
        });
    }

    public static updateCurrentNetwork(callback?: (ok: boolean, network?: WifiNetworkDto|null)=>void) {
        WifiApi.getCurrentNetwork(data => {
            if (data === undefined) {
                if(callback) callback(false);
                return;
            }

            WifiManager._currentNetwork = data;
            if(callback) callback(true, WifiManager._currentNetwork);
            WifiManager.callEvent(WifiManager._currentNetworkChangedEvent, [WifiManager._currentNetwork]);
        });
    }

    public static connect(ssid: string, password: string, callback?: (ok: boolean)=>void) {
        WifiApi.connect(ssid, password, (ok) => {
            WifiManager.updateCurrentNetwork(okUCN => {
                // TODO: what to do if updating the current network failed
                if(callback) callback(ok)
                WifiManager.callEvent(WifiManager._connectedToWifiEvent, [ok]);
            });
        });
    }

    public static disconnect(callback?: (ok: boolean)=>void) {
        WifiApi.disconnect((ok) => {
            WifiManager._currentNetwork = null;
            if(callback) callback(ok);
            WifiManager.callEvent(WifiManager._disconnectedFromWifiEvent, [ok]);
            WifiManager.callEvent(WifiManager._currentNetworkChangedEvent, [WifiManager._currentNetwork]);
        });
    }

    public static getDetectedNetworks = () => WifiManager._detectedNetworks;
    public static getCurrentNetwork = () => WifiManager._currentNetwork;

    private static callEvent = (event: ((...params: any)=>void)[], data: any) => event.forEach(e => e(...data));
    private static _detectedNetworksChangedEvent: ((networks: WifiNetworkDto[])=>void)[] = [];
    private static _currentNetworkChangedEvent: ((network: WifiNetworkDto)=>void)[] = [];
    private static _connectedToWifiEvent: ((ok: boolean)=>void)[] = [];
    private static _disconnectedFromWifiEvent: ((ok: boolean)=>void)[] = [];
    public static subscribeDetectedNetworksChanged = (callback: (networks: WifiNetworkDto[])=>void) => WifiManager._detectedNetworksChangedEvent.push(callback);
    public static subscribeCurrentNetworkChanged = (callback: (networks: WifiNetworkDto)=>void) => WifiManager._currentNetworkChangedEvent.push(callback);
    public static subscribeConnectedToWifi = (callback: (ok: boolean)=>void) => WifiManager._connectedToWifiEvent.push(callback);
    public static subscribeDisconnectFromWifi = (callback: (ok: boolean)=>void) => WifiManager._disconnectedFromWifiEvent.push(callback);
}