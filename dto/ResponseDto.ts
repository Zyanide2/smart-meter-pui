export type ResponseDto<T> = {
    status: number,
    ok: boolean,
    message?: string,
    body?: T,
    error?: string
};