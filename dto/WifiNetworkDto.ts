export type WifiNetworkDto = {
    ssid: string,
    quality: number,
    hasSecurity: boolean
};