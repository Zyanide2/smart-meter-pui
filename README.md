# beurtbalkje-receiver
__
The following guide is meant for RaspBerry-Pi OS 32bit:
Clone this repo to your RaspBerry-Pi in `/etc` & `cd` into the directory:
```
cd /etc/smart-meter-pui
```

Make sure you have Network-Manager correctly installed by running:
```
sudo apt install network-manager network-manager-gnome openvpn \ openvpn-systemd-resolved network-manager-openvpn \ network-manager-openvpn-gnome && sudo apt purge openresolv dhcpcd5 -y
```

Now install the required lib-packages & build the project by running:
```
npm install && npm run build
```

To show the server in kiosk mode install chomium-browser by running:
```
sudo apt-get install chromium-browser -y
```

edit `/etc/xdg/lxsession/LXDE-pi/autostart` to the following script:
```
@chromium-browser --kiosk --incognito --disable-pinch --overscroll-history-navigation=0 http://localhost:3000
```

in `/etc/lightdm/lightdm.conf` add the following command:
```
xserver-command=X -nocursor
```

add the path to `startServer.sh` from the project to `/etc/rc.local` just before `exit 0`.
In our case it would look like:
```
/etc/beurtbalkje-receiver/startServer.sh

exit 0
```

Make `startServer.sh` executable by running:
```
sudo chmod +x /etc/beurtbalkje-receiver/startServer.sh
```

Now your OS will autostart the django server and open it's index page in kiosk mode at startup
