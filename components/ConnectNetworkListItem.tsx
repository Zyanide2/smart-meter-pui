import type {NextPage} from "next";
import {WifiNetworkDto} from "../dto/WifiNetworkDto";
import {WifiManager} from "../utils/WifiManager";
import {KeyboardEventHandler, useEffect, useState} from "react";
import {Lock, Wifi} from "react-feather";
import styles from './ConnectNetworkListItem.module.css';
import {KeyboardManager} from "../utils/KeyboardManager";

type ConnectNetworkListItem = {
    network: WifiNetworkDto
    selectNetwork: ()=>void,
    selectedSSID: string|undefined,
    password: string
    setPassword: (password: string)=>void,
    connect: ()=>void
}

const ConnectNetworkListItem: NextPage<ConnectNetworkListItem> = (props) => {
    const wifiSize = 5+20*props.network.quality/100; // minSize=5; sizeRange=20; gives maxSize of 25
    const itemState = props.selectedSSID == props.network.ssid ? styles['expanded'] : styles['collapsed'];
    const onEnterConnect = (e: any) => {
        if(e.key === 'Enter') props.connect();
    };

    return (
        <div className={styles['container']} onClick={props.selectNetwork}>
            <div className={styles['header-container']}>
                <div className={styles['network-id-container']}>
                    <div className={styles['ssid']}>{props.network.ssid || 'Hidden Network'}</div>
                    {props.network.hasSecurity ? <Lock className={styles['security-icon']} /> : ''}
                </div>
                <Wifi size={wifiSize} className={styles['wifi-icon']} />
            </div>
            <div className={`${styles['body-container']} ${itemState}`}>
                <input type="password"
                       value={props.password}
                       onChange={e => props.setPassword(e.target.value)}
                       onFocus={() => KeyboardManager.showKeyboard(props.setPassword)}
                       placeholder='Enter password'
                       className={styles['password-field']}
                       onKeyDown={onEnterConnect}
                />
                <button onClick={props.connect} className={styles['connect-btn']}>Connect</button>
            </div>
        </div>
    );
};

export default ConnectNetworkListItem;