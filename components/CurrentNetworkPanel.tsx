import type {NextPage} from "next";
import {WifiNetworkDto} from "../dto/WifiNetworkDto";
import {WifiManager} from "../utils/WifiManager";
import styles from './CurrentNetworkPanel.module.css';
import ConnectNetworkListItem from "./ConnectNetworkListItem";
import {Wifi} from "react-feather";

const CurrentNetworkPanel: NextPage<WifiNetworkDto> = (props) => {
    const disconnect = () => WifiManager.disconnect();
    const wifiSize = (5+20*props.quality/100)*3.5; // minSize=5; sizeRange=20; gives maxSize of 25

    return (
        <div className={styles['container']}>
            <span className={styles['title']}>You are connected to:</span>
            <div className={styles['ssid']}>{props.ssid}</div>
            <button onClick={disconnect} className={styles['disconnect-btn']}>Disconnect</button>
            <div className={styles['wifi-icon-container']}>
                <Wifi className={styles['wifi-icon']} size={wifiSize} />
            </div>
        </div>
    );
};

export default CurrentNetworkPanel;