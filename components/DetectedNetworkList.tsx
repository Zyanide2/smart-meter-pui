import type {NextPage} from "next";
import {WifiNetworkDto} from "../dto/WifiNetworkDto";
import ConnectNetworkListItem from "./ConnectNetworkListItem";
import {useEffect, useState} from "react";
import styles from './DetectedNetworkList.module.css';
import {KeyboardManager} from "../utils/KeyboardManager";
import {WifiManager} from "../utils/WifiManager";

type DetectedNetworkListProps = {
    detectedNetworks: WifiNetworkDto[]
}

let SSID: string|undefined = '';
let PWD = '';
const DetectedNetworkList: NextPage<DetectedNetworkListProps> = (props) => {
    const [selectedSSID, setSelectedSSID] = useState<string|undefined>();
    const [password, setPassword] = useState<string>('');
    const changePwd = (pwd: string) => {
        setPassword(pwd);
        PWD = pwd;
    }
    const connect = () => {
        WifiManager.connect(SSID || '', PWD);
        KeyboardManager.hideKeyboard();
    };

    useEffect(() => {
        KeyboardManager.resetEnterIsPressedSubscriptions();
        KeyboardManager.subscribeEnterIsPressed(() => connect());
    }, [props.detectedNetworks])

    useEffect(() => {
        changePwd('');
        SSID = selectedSSID;
    }, [selectedSSID]);

    return (
        <div className={styles['container']}>
            <span className={styles['title']}>Connect to a network</span>
            <div className={styles['list-container']}>
                {props.detectedNetworks.length != 0
                    ? props.detectedNetworks.map((network, i) => (
                        <ConnectNetworkListItem key={i} connect={() => connect()} password={password} setPassword={changePwd} selectedSSID={selectedSSID} network={network} selectNetwork={() => {
                            console.log(`Select SSID: ${network.ssid}`);
                            setSelectedSSID(network.ssid);
                        }}/>
                    ))
                    : <div className='mx-5 my-1'>
                        No networks found A.T.M.
                    </div>
                }
            </div>
        </div>
    );
};

export default DetectedNetworkList;